const koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const app = new koa()
const router = new Router()
const path = require('path')
app.use(serve(path.join(__dirname, 'public')))

router.get('/', async ctx => {
    ctx.myVariable = 'forbidden'; // assume variable
    await next();
})
async function myMiddleware(ctx, next) {
    if (ctx.myVariable == 'forbidden')
        ctx.body = "Access Denied";
    else
        await next();
}
app.use(myMiddleware)
app.use(router.routes())
app.use(myMiddleware)
app.use(myMiddleware)
app.listen(3000)
