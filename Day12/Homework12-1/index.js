const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
});

router.get('/staff', async ctx => {
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'bookstore'
        })

    const [rows, fields] = await connection.query('SELECT * FROM staff')
    const data1 = { rows }
    //console.log(data1)

    await ctx.render('staff', data1)
})

router.get('/book', async ctx => {
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'bookstore'
        })

    const [rows, fields] = await connection.query('SELECT * FROM book')
    const data2 = { rows }
    //console.log(data2)

    await ctx.render('book', data2)
})

router.get('/salelist', async ctx => {
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'bookstore'
        })

    const [rows, fields] = await connection.query('SELECT * FROM saleslist')
    const data3 = { rows }
    console.log(data3)

    await ctx.render('salelists', data3)
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
