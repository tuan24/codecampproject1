const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async ctx => {
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'db2'
        }
    )
    const [rows, fields] = await connection.query('SELECT c.id, c.name as course_name, i.name as instructor_name FROM courses as c RIGHT JOIN instructors as i on i.id = c.teach_by where c.id is null ORDER BY c.id')

    const [rows2, fields2] = await connection.query('SELECT c.id, c.name as course_name, i.name as instructor_name FROM courses as c LEFT JOIN instructors as i on i.id = c.teach_by WHERE i.id is null ORDER BY c.id;')

    await ctx.render('homework13-1', { rows, rows2 })

})


app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)