const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
});


router.get('/', async ctx => {
    const connection = await mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'db2'
        })

    const [data, fields] = await connection.query('select name, price from courses inner join enrolls on enrolls.course_id = courses.id UNION SELECT \'Total total\', SUM(price) from courses inner join enrolls on enrolls.course_id = courses.id;')

    const [data2, fields2] = await connection.query('select id, name, SUM(price) as enroll_price from (select students.id, students.name, courses.price from enrolls right join students on students.id = enrolls.student_id left join courses on courses.id = enrolls.course_id) as t GROUP BY id;')
    //console.log({ data })

    await ctx.render('landing', { data, data2 })

})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)

