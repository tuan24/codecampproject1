const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()



router.get('/instructor/find_all', async (ctx, next) => {
    const pool = await mysql.createPool(
        {
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'design_pattern'
        }
    )
    const [rows] = await pool.query('SELECT * from instructors')
    ctx.body = rows
    await next()
})

router.get('/instructor/find_by_id/:id', async (ctx, next) => {
    const pool = await mysql.createPool(
        {
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'design_pattern'
        }
    )
    const [rows] = await pool.query('SELECT * from instructors WHERE id = ?', [ctx.params.id])
    ctx.body = rows
    await next()
})

router.get('/course/find_by_id/:id', async (ctx, next) => {
    const pool = await mysql.createPool(
        {
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'design_pattern'
        }
    )
    const [rows] = await pool.query('SELECT * from courses WHERE id = ?', [ctx.params.id])
    ctx.body = rows
    await next()
})

router.get('/course/find_by_price/:price', async (ctx, next) => {
    const pool = await mysql.createPool(
        {
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'design_pattern'
        }
    )
    const [rows] = await pool.query('SELECT * from courses WHERE price = ?', [ctx.params.price])
    ctx.body = rows
    await next()
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)

