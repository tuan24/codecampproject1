const Koa = require('koa')

const app = new Koa()

app.use(ctx => {
    ctx.status = 200
    ctx.set('Content-Type', 'text/html; charset=utf-8')
    ctx.set('Cache-Control', 'private, max-age=0')
    ctx.body = '<h1>Hello World</h1>'
})

app.listen(3000)