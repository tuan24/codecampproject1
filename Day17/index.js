const Koa = require('koa')
const koaBody = require('koa-body')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/signin', signinGetHandler)
router.post('/signin', signinPostHandler)

async function signinGetHandler(ctx) {
    await ctx.render('signin')
}

function signinPostHandler(ctx) {
    console.log('email: ', ctx.request.body.email)
    console.log('password: ', ctx.request.body.password)
    ctx.redirect('/signin')
}

router.get('/signup', signupGetHandler)
router.post('/signup', signupPostHandler)

async function signupGetHandler(ctx) {
    await ctx.render('signup')
}

async function signupPostHandler(ctx) {
    const pool = await mysql.createPool(
        {
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: 'm;o',
            database: 'pikkanode'
        }
    )

    await pool.query('insert into users (email, password) values ("' + ctx.request.body.email + '", "' + ctx.request.body.password + '")')

    const [rows] = await pool.query('SELECT * from users')
    ctx.body = rows
}

app.use(serve(path.join(__dirname, 'public')))
app.use(koaBody())
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
