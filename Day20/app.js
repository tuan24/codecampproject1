const Koa = require('koa');
const Router = require('koa-router')
const render = require('koa-ejs');
const koaBody = require('koa-body')
const session = require('koa-session')
const path = require('path');
const mysql = require('mysql2/promise')

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'm;o',
    database: 'fblogin'
});

const app = new Koa();
const router = new Router()

app.use(koaBody())

app.keys = ['some secret hurr asdf qwer']
const sessionStore = {}

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    maxAge: 86400000,
    autoCommit: true, /** (boolean) automatically commit headers (default true) */
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key]
        },
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess
        },
        destroy(key) {
            delete sessionStore[key]
        }
    }
}

app.use(session(CONFIG, app))

render(app, {
    root: path.join(__dirname, 'views'),
    layout: false,
    viewExt: 'ejs',
    cache: false
});

router.get('/', async ctx => {
    await ctx.render('index');
});



router.post('/facebook_login', async ctx => {
    const email = ctx.request.body.fbEmail
    const getLogin = async (email) => {
        const [results, fields] = await pool.query(
            `select facebook_user_id from user where email=?`, [email])
        if (results[0] == undefined) {
            return undefined
        }
        return results[0].facebook_user_id
    }

    const fb = await getLogin(email)
    if (fb != undefined) {
        ctx.session.userId = fb
        ctx.redirect('/profile')
    } else {
        const [results, fields] = await pool.query(`
    INSERT INTO user (facebook_user_id, firstname, lastname, email) VALUES (?, ?, ?, ?)
    `, [ctx.request.body.fbID, ctx.request.body.fbName, ctx.request.body.fbLastName, ctx.request.body.fbEmail])
        await ctx.render('register_completed')
    }
})

router.get('/profile', async (ctx, next) => {
    const [results, fields] = await pool.query(`
    SELECT facebook_user_id, firstname
    FROM user
    WHERE facebook_user_id = ?
    `, [ctx.session.userId])
    await ctx.render('profile', {
        id: results[0].facebook_user_id,
        name: results[0].firstname
    })
    await next()
})

app.use(router.routes())
app.listen(3000)
