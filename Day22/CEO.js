const { Employee } = require('./Employee.js')

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        // this._fire = function () {
        //     console.log(Employee.firstname + 'Has been fired!')
        // }
    }
    getSalary() {  // simulate public method
        return super.getSalary() * 2;
    }
    work(employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();

    }
    increaseSalary(employee, newSalary) {
        let checkSalary = employee.setSalary(newSalary)
        if (checkSalary) {
            console.log(employee.firstname + "\'s salary has been set to " + newSalary)
        } else {
            console.log(employee.firstname + "\'s salary less than before!!")
        }

    }
    _seminar() {
        console.log("He is going to seminar" + " Dress with: " + this.dressCode)
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    }
    _fire(employee) {
        console.log(employee.firstname + " has been fire!" + " Dress with: " + employee.dressCode)
    }
    _hire(employee) {
        console.log(employee.firstname + " has been hired back!" + " Dress with: " + employee.dressCode)
    }

}

module.exports.CEO = CEO