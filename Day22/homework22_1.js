const { Employee } = require('./Employee.js')
const { CEO } = require('./CEO.js')



let somchai = new CEO("Somchai", "Sudlor", 30000);
let somsri = new Employee("Somsri", "Sudsuay", 22000);
somsri.gossip();
somchai.work(somsri);

somchai.increaseSalary(somsri, 20);
somchai.increaseSalary(somsri, 35000);