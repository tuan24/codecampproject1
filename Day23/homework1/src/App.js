import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './Hello1'
import './Hello2'
import Hello1 from './Hello1';
import Hello2 from './Hello2';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div>
          <Hello1 />
        </div>
        <div>
          <Hello2 />
        </div>


      </div>
    );
  }
}

export default App;
