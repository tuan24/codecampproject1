import React from 'react'

class PictureCard extends React.Component {
    render() {
        return (
            <div>
                <img src={this.props.imgSrc}></img>
                <p>ID: {this.props.id}</p>
                <p>Create By: {this.props.createBy}</p>
                <p>Post Date: {this.props.date.toString()}</p>
                <p>Like: {this.props.likeCount}</p>
                <p>Comment: {this.props.commentCount}</p>
            </div>
        )
    }
}



export default PictureCard