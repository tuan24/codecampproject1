import React from 'react'
import PictureCard from './PictureCard'
import PictureCard2 from './PictureCard'
import PictureCard3 from './PictureCard'
import PictureCard4 from './PictureCard'
import PictureCard5 from './PictureCard'
import './Pikkanode.css'

class Pikkanode extends React.Component {
    render() {
        const now = new Date()

        return (
            <div>
                <div class='PictureCard'>
                    <PictureCard
                        imgSrc={'https://cdn.newsapi.com.au/image/v1/9fdbf585d17c95f7a31ccacdb6466af9'}
                        id={1}
                        createBy={'Tuan1'}
                        date={now}
                        likeCount={100}
                        commentCount={20}
                    />
                </div>

                <div class='PictureCard2'>
                    <PictureCard2
                        id={2}
                        imgSrc={'https://steemitimages.com/DQmfMNupYMHmp11bgDW58J41ToPSVTvyoFb9x8KrjHEGfnE/POTD_chick_3597497k.jpg'}
                        createBy={'Tuan2'}
                        date={now}
                        likeCount={50}
                        commentCount={10}
                    />
                </div>

                <div class='PictureCard3'>
                    <PictureCard3
                        id={3}
                        imgSrc={'https://media.istockphoto.com/photos/tulips-picture-id511112100?k=6&m=511112100&s=612x612&w=0&h=hOMlLwDwGy1tkQYofgEMcCFDRT0QdsbgGaK4HPiSwjU='}
                        createBy={'Tuan3'}
                        date={now}
                        likeCount={150}
                        commentCount={100}
                    />
                </div>

                <div class='PictureCard4'>
                    <PictureCard4
                        id={4}
                        imgSrc={'http://www.theamazingpics.com/wp-content/uploads/2014/05/Amazing-Picture-of-A-Japanese-Garden-in-Portland-USA.jpg'}
                        createBy={'Tuan4'}
                        date={now}
                        likeCount={200}
                        commentCount={150}
                    />
                </div>

                <div class='PictureCard5'>
                    <PictureCard5
                        id={5}
                        imgSrc={'https://media.istockphoto.com/photos/grey-squirrel-yawning-picture-id473012660?k=6&m=473012660&s=612x612&w=0&h=opzkOsCuudeI_l83My-WdfTiru2mpuwZMpVomymwC9c='}
                        createBy={'Tuan5'}
                        date={now}
                        likeCount={30}
                        commentCount={10}
                    />
                </div>
            </div>
        )
    }
}




export default Pikkanode