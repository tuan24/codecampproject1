import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Pikkanode from './Pikkanode'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render([<App />, <Pikkanode />], document.getElementById('root'));
registerServiceWorker();
