import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import Main from './Main'
import Navbar from './Navbar'

class App extends Component {
  render() {
    return (
      <div>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
        </div>
        <div>
          <Navbar />
          <Main />
        </div>
      </div >
    );
  }
}

export default App;

