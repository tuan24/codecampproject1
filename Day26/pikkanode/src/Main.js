import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Signup from './pages/Signup'
import Home from './pages/Home'

const Main = () => (
    <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/signup' component={Signup} />
    </Switch>
)

export default Main