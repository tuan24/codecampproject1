import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => (
    <div className="Navbar">
        <div className="Home"><Link to='/'>Home</Link></div>
        <div className="Signup"><Link to='/signup'>Signup</Link></div>
    </div>
)

export default Navbar