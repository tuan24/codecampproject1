import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

class Signup extends React.Component {
    state = {
        email: '',
        password: ''
    }


    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }


    handleSubmit = async e => {
        e.preventDefault()
        console.log(this.state)
        try {
            const response = await fetch('http://guver.net/api/v1/auth/signup', {
                method: 'post',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify({ email: this.state.email, password: this.state.password }),
                credentials: 'include'
            })
            const data = await response.json()
            console.log(data)
        } catch (err) {
            console.log(err)
        }
    }

    render() {
        const {
            email,
            password
        } = this.state

        return (
            <div className="flex-container">
                <div className="container">
                    <form onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col-25">
                                <label htmlFor="email">Email:</label>
                            </div>
                            <div className="col-75">
                                <input type='email' name='email' value={email} onChange={this.handleChange} placeholder='Email' required />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-25">
                                <label htmlFor="password">Password:</label>
                            </div>
                            <div class="col-75">
                                <input type='password' name='password' onChange={this.handleChange} placeholder='Password' required />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-25">
                                <label htmlFor="conpassword">Confirm Password: </label>
                            </div>
                            <div className="col-75">
                                <input type='password' name='password' placeholder='Password' required />
                            </div>
                        </div>
                        <div className="row">
                            <input type="submit" value="Signup" />
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}



export default Signup
