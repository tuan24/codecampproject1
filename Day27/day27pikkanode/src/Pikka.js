import React, { Component } from 'react'
import ReactDOM from 'react-dom'

class Pikka extends Component {

    state = {
        pikka: [],
        // id: '',
        // caption: '',
        // picture: '',
        // createdAt: '',
        // commentCount: '',
        // liked: '',
        // likeCount: '',
        // createdBy: '',
        isLoading: true,
    }

    async componentDidMount() {
        try {
            const response = await fetch('http://guver.net/api/v1/pikka')
            const { list } = await response.json()
            console.log(list)

            this.setState({
                pikka: list,
                // id: list.id,
                // caption: list.caption,
                // picture: list.picture,
                // createdAt: list.createdAt,
                // commentCount: list.commentCount,
                // liked: list.liked,
                // likeCount: list.likeCount,
                // createdBy: list.createdBy
                //isLoading: false
            })


        } catch (err) {
            console.log(err.message)
        } finally {
            this.setState({ isLoading: false })
        }
    }

    render() {
        //const { id, caption, createdBy, createdAt, commentCount, likeCount, liked, picture, isLoading } = this.state
        const { pikka, isLoading } = this.state

        if (isLoading) return <div>Loading...</div>
        return (pikka.map(pikka =>
            <div className="container">
                <div className="row">
                    <div className="col-sm-12 col-md-6 col-lg-3 image-list" >
                        <img src={pikka.picture}></img> <br />
                        id: {pikka.id} <br />
                        caption: {pikka.caption} <br />
                        createdBy: {pikka.createdBy} <br />
                        createdAt: {pikka.createdAt} <br />
                        commentCount: {pikka.commentCount} <br />
                        likeCount: {pikka.likeCount} <br />
                        liked: {pikka.liked.toString()} <br />
                    </div>
                </div>
            </div>
        ))
    }
}

export default Pikka