const mysql = require('mysql2/promise');
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'm;o',
    database: 'koa_login'
});
const tempUserController = require('../controller/user')
const userController = tempUserController(null, pool)
const assert = require('assert')

describe('userController', () => {
    describe('#insertUser', () => {
        it('should return with empty errorMessage', async () => {
            const ctx = {
                request: {
                    body: {
                        username: 'myUsername',
                        password: '1234',
                        email: 'a@a.com'
                    }
                },
                render: async function (somePage, dataObject) {

                }
            }

            async function next() {
                return true
            }

            const dataSend = await userController.insertUser(ctx, next)
            assert.deepEqual(dataSend.errorMessage, '')
        })
    })
})