let fs = require('fs')
let assert = require('assert')

function myReadFile2(filename) {
    return new Promise(function (resolve, reject) {
        fs.readFile(filename, 'utf8', function (err, dataDemo1) {
            if (err)
                reject(err);
            else
                resolve(dataDemo1);
        });
    })
}

describe('Async Test2', function () {
    describe('#myReadFile2()', function () {
        it('should read content of the file out', async function () {
            const data = await myReadFile2("demofile1.txt");
            assert.strictEqual(data.length > 0, true, "demofile1.txt should not be empty");
        });
    });
});

