'use strict'

let fs = require('fs');

fs.writeFileSync('demofile1.txt', 'test content', 'utf8');
fs.writeFile('demofile1.txt', 'test content', 'utf8', (err) => {
    console.log('write complete');
});

let fileContent = fs.readFileSync('demofile1.txt', 'utf8');
console.log(fileContent);

fs.readFile('demofile1.txt', 'utf8', (err, data) => {
    console.log(data);
});

