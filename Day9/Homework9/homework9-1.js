let fs = require('fs')

function writeRobot() {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', '', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve()
        })
    })
}

function readHead() {
    return new Promise(function (resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, head) {
            if (err)
                reject(err)
            else
                resolve(head)

        })
    })
}
function writeHead(head) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', head + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(head)
            console.log(head)
        })
    })
}

function readBody() {
    return new Promise(function (resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, body) {
            if (err)
                reject(err)
            else
                resolve(body)
        })
    })
}
function writeBody(body) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', body + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(body)
            console.log(body)
        })
    })
}

function readLeg() {
    return new Promise(function (resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, leg) {
            if (err)
                reject(err)
            else
                resolve(leg)
        })
    })
}
function writeLeg(leg) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', leg + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(leg)
            console.log(leg)
        })
    })
}

function readFeet() {
    return new Promise(function (resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, feet) {
            if (err)
                reject(err)
            else
                resolve(feet)
        })
    })
}
function writeFeet(feet) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', feet + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(feet)
            console.log(feet)
        })
    })
}

async function makeRobot() {
    try {
        await writeRobot()
        let head = await readHead()
        await writeHead(head)
        let body = await readBody()
        await writeBody(body)
        let leg = await readLeg()
        await writeLeg(leg)
        let feet = await readFeet()
        await writeFeet(feet)
    } catch (error) {
        console.error(error)
    }
}
makeRobot()


/*writeRobot().then(function () {
    return readHead()
}).then(function (readHead) {
    return writeHead(readHead)
}).then(function () {
    return readBody()
}).then(function (readBody) {
    return writeBody(readBody)
}).then(function () {
    return readLeg()
}).then(function (readLeg) {
    return writeLeg(readLeg)
}).then(function () {
    return readFeet()
}).then(function (readFeet) {
    return writeFeet(readFeet)
}).then(function (err) {
    console.log(err)
})*/

