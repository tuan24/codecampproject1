let fs = require('fs')

/*function writeRobot() {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', '', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve()
        })
    })
}*/

let p1 = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function (err, head) {
        if (err)
            reject(err)
        else
            resolve(head)

    })
})

function writeHead(head) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', head + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(head)
            console.log(head)
        })
    })
}


let p2 = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function (err, body) {
        if (err)
            reject(err)
        else
            resolve(body)
    })
})

function writeBody(body) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', body + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(body)
            console.log(body)
        })
    })
}

let p3 = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function (err, leg) {
        if (err)
            reject(err)
        else
            resolve(leg)
    })
})

function writeLeg(leg) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', leg + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(leg)
            console.log(leg)
        })
    })
}

let p4 = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function (err, feet) {
        if (err)
            reject(err)
        else
            resolve(feet)
    })
})

function writeFeet(feet) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('robot.txt', feet + '\n', 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve(feet)
            console.log(feet)
        })
    })
}

function writeRobot(data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', data, 'utf8', function (err) {
            if (err)
                reject(err)
            else
                resolve()
        })
    })
}

Promise.all([p1, p2, p3, p4])
    .then(function (result) {
        console.log('All completed!: ', result)
        let data = ''
        for (let i = 0; i < result.length; i++) {
            data += result[i] + '\n'
        }
        return writeRobot(data)
    })
    .catch(function (error) {
        console.error("There's an error", error)
    });

/*async function makeRobot() {
    try {
        await writeRobot()
        let head = await readHead()
        await writeHead(head)
        let body = await readBody()
        await writeBody(body)
        let leg = await readLeg()
        await writeLeg(leg)
        let feet = await readFeet()
        await writeFeet(feet)
    } catch (error) {
        console.error(error)
    }
}
makeRobot()*/