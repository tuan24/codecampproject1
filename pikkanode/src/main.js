const Koa = require('koa')
const koaBody = require('koa-body')
const serve = require('koa-static')
const session = require('koa-session')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs-extra')
const cors = require('@koa/cors')


const app = new Koa()



const sessionStore = {}
const sessionConfig = {
	key: 'sess',
	maxAge: 1000 * 60 * 60,
	httpOnly: true,
	store: {
		get(key, maxAge, { rolling }) {
			return sessionStore[key]
		},
		set(key, sess, maxAge, { rolling }) {
			sessionStore[key] = sess
		},
		destroy(key) {
			delete sessionStore[key]
		}
	}
}
const flash = async (ctx, next) => {
	if (!ctx.session) throw new Error('flash message required session')
	ctx.flash = ctx.session.flash
	delete ctx.session.flash
	await next()
}

render(app, {
	root: path.join(process.cwd(), 'views'),
	layout: 'template',
	viewExt: 'ejs',
	cache: false
});

const stripPrefix = async (ctx, next) => {
	if (!ctx.path.startsWith('/-')) {
		ctx.status = 404
		return
	}

	ctx.path = ctx.path.slice(2)
	await next()
}



app.use(cors({
	origin: 'https://panotza.github.io',
	allowMethods: ['GET', 'POST', 'PUT', 'DELETE'],
	allowHeaders: ['Authorization', 'Content-Type'],
	maxAge: 10,
	credentials: true
}))
app.use(koaBody({ multipart: true }))
app.keys = ['supersecret']
app.use(session(sessionConfig, app))
app.use(flash)
app.use(require('./route'))
app.use(stripPrefix)
app.use(serve(path.join(process.cwd(), 'public')))


app.listen(3000)
