
const pool = require('../db')

const register = async (email, hashedPassword) => {
    const result = await pool.query(`
		insert into users
			(email, password)
		values
			(?, ?)
	`, [email, hashedPassword])
    //console.log(result)

    // ????
    return result[0].insertId
}

module.exports = {
    register
}
