
const pool = require('../db')

const signupEmail = async (email) => {
	const result = await pool.query(
		'select id from users where email=?', [email])
	if (result[0][0] == undefined) {
		return undefined
	}
	return result[0][0].id
}

const register = async (email, hashedPassword) => {
	const result = await pool.query(`
		insert into users
			(email, password)
		values
			(?, ?)
	`, [email, hashedPassword])
	//console.log(result)

	// ????
	return result[0].insertId
}






const getPass = async (email) => {
	const result = await pool.query(
		`select password from users where email=?`, [email])
	if (result[0][0] == undefined) {
		return undefined
	}
	return result[0][0].password
}

const getIdFromEmail = async (email) => {
	const result = await pool.query(
		'select id from users where email=?', [email])
	return result[0][0].id
}


module.exports = {
	register,
	getPass,
	getIdFromEmail,
	signupEmail,
	//facebookLogin,

}
