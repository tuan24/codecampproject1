const Koa = require('koa')
const path = require('path')
const Router = require('koa-router')



const signIn = require('./signin')
const signUp = require('./signup')
const signOut = require('./signout')

const app = new Koa()
const router = new Router()



router.get('/', signIn.checkAuth, ctx => {
    ctx.body = 'you are signed in'
})

router.get('/signin', signIn.getHandler)
router.post('/signin', signIn.postHandler)
router.get('/signup', signUp.getHandler)
router.post('/api/v1/auth/signup', signUp.postHandler)
router.post('/api/v1/auth/signout', signOut.postHandler)




module.exports = router.routes()
