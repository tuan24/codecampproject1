const { user } = require('../../repository')

const bcrypt = require('bcrypt')


const getHandler = async (ctx) => {
	const data = {
		flash: ctx.flash
	}

	await ctx.render('signin', data)
}


const postHandler = async (ctx) => {


	// if (!ctx.request.body.email) {
	// 	ctx.session.flash = { error: 'email require' }
	// 	ctx.status = 400
	// 	ctx.body = {
	// 		error: 'email required'
	// 	}
	// 	return ctx
	// }
	// else if (!ctx.request.body.password) {
	// 	ctx.session.flash = { error: 'password require' }
	// 	ctx.status = 400
	// 	ctx.body = {
	// 		error: 'password required'
	// 	}
	// 	return ctx
	// } else {
	// 	ctx.status = 200
	// 	ctx.body = {}
	// }



	const email = ctx.request.body.email
	const password = ctx.request.body.password


	const hashedPassword = await user.getPass(email)
	if (hashedPassword == undefined) {
		ctx.session.flash = { error: 'email does not exist' }
		return ctx.redirect('/signin')
	}

	const same = await bcrypt.compare(password, hashedPassword)
	if (!same) {
		ctx.session.flash = { error: 'wrong password' }
		return ctx.redirect('/signin')
	}

	ctx.session.userId = await user.getIdFromEmail(email)

	//ctx.session.userId2 = await user.facebookLogin(fb_id, firstName, lastName)

	await ctx.redirect('/')
}

const checkAuth = async (ctx, next) => {
	if (!ctx.session || !ctx.session.userId) {
		ctx.body = `<p>you are not signed in</p>
			<a href="/signin">signin here</a>`
		return
	}
	await next()
}




module.exports = {
	getHandler,
	postHandler,
	checkAuth
}
