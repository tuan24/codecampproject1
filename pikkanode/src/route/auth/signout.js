const getHandler = async (ctx) => {
    const data = {
        flash: ctx.flash
    }
    await ctx.render('signout', data)
}


const postHandler = async (ctx) => {
    ctx.status = 400
    ctx.body = {}
    return ctx

}


module.exports = {
    getHandler,
    postHandler
}