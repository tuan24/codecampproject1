const { user } = require('../../repository')

const bcrypt = require('bcrypt')

const getHandler = async (ctx) => {
	const data = {
		flash: ctx.flash
	}
	await ctx.render('signup', data)
}


const postHandler = async (ctx) => {
	const { email, password } = ctx.request.body

	if (!email) {
		ctx.status = 400
		ctx.body = {
			error: 'email required'
		}
		return ctx
	}

	if (!password) {
		ctx.status = 400
		ctx.body = {
			error: 'password required'
		}
		return ctx
	}


	const checkEmail = await user.signupEmail(email)
	//console.log(checkEmail)
	if (checkEmail !== undefined) {
		ctx.session.flash = { error: 'email already signup' }
		return ctx.redirect('/signup')
	}

	const hashedPassword = await bcrypt.hash(ctx.request.body.password, 10)


	if (checkEmail == undefined) {
		ctx.session.flash = { error: 'signup complete' }
		return ctx.redirect('/signup')
	}

	// TODO: validate email, password

	const userId = await user.register(email, hashedPassword)


	ctx.redirect('/signup')

	// TODO: handle user id ?
}

module.exports = {
	getHandler,
	postHandler
}
