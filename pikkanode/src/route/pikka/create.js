const { picture } = require('../../repository')

const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')
const path = require('path')

const getHandler = async (ctx) => {
	await ctx.render('upload')
}

const pictureDir = path.join(process.cwd(), 'public', 'images')

const allowFileType = {
	'image/png': true,
	'image/jpeg': true
}

const uploadHandler = async ctx => {
	try {
		if (!allowFileType[ctx.request.files.photo.type]) {
			throw new Error('file type not allow')
		}
		console.log(ctx.request.body.caption)
		console.log(ctx.request.body.detail)
		console.log(ctx.request.files.photo.name)
		console.log(ctx.request.files.photo.path)

		const fileName = uuidv4()
		await fs.rename(ctx.request.files.photo.path, path.join(pictureDir, fileName))
		ctx.status = 303
		ctx.redirect('/create')
	} catch (err) {
		// handle error here
		ctx.status = 400
		ctx.body = err.message
		fs.remove(ctx.request.files.photo.path)
	}
}



module.exports = {
	getHandler,
	uploadHandler
}
